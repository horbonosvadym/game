<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Game</title>
  <?php require_once($_SERVER['DOCUMENT_ROOT'] . '/scripts/get_bundle_name.php') ?>

  <link rel="stylesheet" href="<?php echo get_bundle_name()['css'] ?>">
</head>
<body>
  <div class="o-site">
    <div class="o-flex js-scope">
      <h1 class="t1 u-mt u-midnight-color">Game</h1>
      <div class="c-form ">
      
        <div class="c-form__inner">
          <div class="c-form__group c-form__group--num">
            <label class="c-form__label">Time in miliseconds</label>
            <input class="js-time" type="number" value="2000" min="0">
          </div>
          <div class="c-form__group">
            <button class="c-btn js-btn">Start</button>
          </div>
          <div class="c-form__group">
            <div class="c-score">
              <div class="c-score__title">Score:</div>
              <div class="c-score__group">
                <div class="c-score__label">💻 AI</div>
                <div class="c-score__value js-ai-score">0</div>
              </div>
              <div class="c-score__group">
                <div class="c-score__label">👶 you</div>
                <div class="c-score__value js-user-score">0</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      

      <div class="c-chars js-chars">
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
        <div class="c-char js-char"></div>
      </div>
    </div>
  </div>
  <div class="c-modal js-modal-fail ">
    <div class="c-modal__inner">
    <div class="c-modal__close js-modal-close">&times;</div>
      <div class="c-modal__img">
        <img src="/assets/media/game-item-1.gif" alt="looser">
      </div>
      <div class="c-modal__scores js-modal-scores">
          <div class="c-score">
            <div class="c-score__title">Score:</div>
            <div class="c-score__group">
              <div class="c-score__label">💻 AI</div>
              <div class="c-score__value js-ai-modal-score">0</div>
            </div>
            <div class="c-score__group">
              <div class="c-score__label">😭 you</div>
              <div class="c-score__value js-user-modal-score">0</div>
            </div>
          </div>

      </div>
    </div>
  </div>
  <div class="c-modal c-modal--success js-modal-succes ">
    <div class="c-modal__inner">
    <div class="c-modal__close js-modal-close">&times;</div>
      <div class="c-modal__img">
        <img src="/assets/media/game-item-0.jpg" alt="winner">
      </div>
      <div class="c-modal__scores js-modal-scores">
          <div class="c-score">
            <div class="c-score__title">Score:</div>
            <div class="c-score__group">
              <div class="c-score__label">💻 AI</div>
              <div class="c-score__value js-ai-modal-score">0</div>
            </div>
            <div class="c-score__group">
              <div class="c-score__label">😀 you</div>
              <div class="c-score__value js-user-modal-score">0</div>
            </div>
          </div>
      </div>
    </div>
  </div>

  <script src="<?php echo get_bundle_name()['js'] ?>"></script>
</body>
</html>