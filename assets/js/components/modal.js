const IS_OPEN = 'is-open';

function toggleModal(selector, btnSelector = '.js-modal-close') {
  const modal = document.querySelector(selector);

  modal.classList.toggle(IS_OPEN);

  const btn = modal.querySelector(btnSelector);

  btn.addEventListener('click', () => modal.classList.remove(IS_OPEN));
}

function updateModalScores(selector, aiScoreSelector, userScoreSelector, scoreAi, scoreUser) {
  const modal = document.querySelector(selector);

  modal.querySelector(aiScoreSelector).innerHTML = scoreAi;
  modal.querySelector(userScoreSelector).innerHTML = scoreUser;
}

export { toggleModal,  updateModalScores};
