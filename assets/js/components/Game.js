import { toggleModal, updateModalScores } from './modal';

const JS_ACTIVE      = 'js-active';
const COLOR_FAIL     = '#c00';
const COLOR_BLACK    = '#000';
const COLOR_SUCCESS  = '#00FF45';
const CSS_IS_SUCCESS = 'is-success';
const CSS_IS_FAIL    = 'is-fail';
const PLAYER_AI      = 'ai';
const PLAYER_USER    = 'user';
 
class Game {
  constructor(props) {
    this.scope          = document.querySelector(props.scopeSelector);
    this.btn            = this.scope && this.scope.querySelector(props.btnSelector);
    this.aiScore        = this.scope && this.scope.querySelector(props.scoreAiSelector);
    this.userScore      = this.scope && this.scope.querySelector(props.scoreUserSelector);
    this.time           = this.scope && this.scope.querySelector(props.timeSelector);
    this.chars          = this.scope && this.scope.querySelectorAll(props.charSelector);
    this.charsContainer = this.scope && this.scope.querySelector(props.charsSelector);
    this.charAmount     = props.charAmount;
    this.winnerPoint    = props.winnerPoint;
    this.clearTimer     = null;
  }

  init() {
    if (!this.scope) {
      return;
    }

    this.bindEvents();
  }

  bindEvents() {
    const _this = this;

    this.btn.addEventListener('click', () => this.btnHandler());

    this.charsContainer.addEventListener('click', function(e) {
      _this.charsHandler(e, _this);
    });
  }

  charsHandler(e, self) {
    const current = e.target;
    const _this   = self;

    if (current.classList.contains(JS_ACTIVE)) {
      clearTimeout(_this.clearTimer);

      // add point to user
      _this.addPoint(PLAYER_USER);

      // check winner
      if (!_this.isWinnerExist(PLAYER_USER)) {

        // make green
        _this.highLight();

        // clear chars active
        _this.removeActive();

        // set another random
        _this.getRandomCharAndMakeActive();
      } else {
        // user won — stop game
        clearTimeout(_this.clearTimer);
        _this.removeActive();
        _this.userScore.style.color = COLOR_SUCCESS;
        // show modal
        updateModalScores('.js-modal-succes', '.js-ai-modal-score', '.js-user-modal-score', _this.aiScore.innerHTML, _this.userScore.innerHTML);
        toggleModal('.js-modal-succes');
      }
    }
  }

  highLight(type = CSS_IS_SUCCESS) {
    const current = this.charsContainer.querySelector(`.${JS_ACTIVE}`);

    current.classList.add(type);
    setTimeout(() => {
      current.classList.remove(type);
    }, 100);
  }

  addPoint(toWho) {
    if (toWho === PLAYER_USER) {
      let userScore = parseInt(this.userScore.innerHTML);

      this.userScore.innerHTML = ++userScore;

      return;
    } 

    let aiScore = parseInt(this.aiScore.innerHTML);
    this.aiScore.innerHTML = ++aiScore;
  }

  btnHandler() {
    // start timer with time value
    // light random char
    this.getRandomCharAndMakeActive();
    this.resetScores();
  }

  getRandomCharAndMakeActive() {
    const randomNum = Math.floor(Math.random() * this.charAmount);

    const currentChar = this.chars[randomNum];

    currentChar.classList.add(JS_ACTIVE); 

    // start time here
    this.startTimer(this.time.value);
  }

  startTimer(time) {
    this.clearTimer = setTimeout(() => {
      // looser here 
      // add point to ai
      this.addPoint(PLAYER_AI);

      // check is winner exist
      if (!this.isWinnerExist(PLAYER_AI)) {
        // show status of raund
        this.highLight(CSS_IS_FAIL);

        // clear chars highlight
        this.removeActive();

        // set another random
        this.getRandomCharAndMakeActive();
      } else {
        // ai won here
        // stop game
        clearTimeout(this.clearTimer);
        this.removeActive();
        this.aiScore.style.color = COLOR_FAIL;
         // show modal
         updateModalScores('.js-modal-fail', '.js-ai-modal-score', '.js-user-modal-score', this.aiScore.innerHTML, this.userScore.innerHTML);
         toggleModal('.js-modal-fail');
      }
    }, time);
  }

  removeActive(active = JS_ACTIVE) {
    this.chars.forEach(char => char.classList.remove(active));
  }

  resetScores() {
    this.aiScore.innerHTML = 0;
    this.aiScore.style.color = COLOR_BLACK;
    this.userScore.innerHTML = 0;
    this.userScore.style.color = COLOR_BLACK;
  }

  isWinnerExist(checkWho) {
    if (checkWho === PLAYER_USER) {
      if(parseInt(this.userScore.innerHTML) >= this.winnerPoint) {
       return true;
      }
      return false;
    } else {
      if(parseInt(this.aiScore.innerHTML) >= this.winnerPoint) {
       return true;
      }
      return false;
    }

  }
}

const options = {
  scopeSelector    : '.js-scope',
  btnSelector      : '.js-btn',
  scoreAiSelector  : '.js-ai-score',
  scoreUserSelector: '.js-user-score',
  timeSelector     : '.js-time',
  charSelector     : '.js-char',
  charsSelector    : '.js-chars',
  charAmount       : 100,
  winnerPoint      : 10
}

const game = new Game(options);

game.init();
